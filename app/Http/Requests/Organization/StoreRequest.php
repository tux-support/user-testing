<?php

namespace App\Http\Requests\Organization;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'unique:organizations', 'max:100']
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Your organization needs a name.',
            'name.unique' => 'Your organization name has already been registered.',
            'name.max' => 'Your organization name is too long. 100 characters is the maximum.'
        ];
    }
}
