<?php

namespace App\Http\Controllers;

use App\Models\Organization;
use App\Models\User;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Organization\StoreRequest;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->has('q')) {
            $organizations = Organization::where('name', 'like', "%{$request->input('q')}%");
        } else {
            $organizations = Organization::query();
        }
        $organizations->paginate(11);
        return response()->view('organization.index', ['organizations' => $organizations->get(), 'q' => $request->has('q')?$request->input('q'):'']);
        /*
        $memberships = Organization::where('id', 10)->first()->getMembershipsByRole('member')->all();
        print("Members:\n");
        print_r($memberships);
        print("Looping:\n");
        foreach($memberships as $membership) {
            print("Loop:\n");
            print_r($membership->member->name);
        }
        */

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->view('organization.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $validatedRequest = $request->validated();
        $organization = Organization::create($validatedRequest);
        $owner = \App\Models\Role::where('name', 'Organization owner')->first();
        $user = Auth::user();
        $team = $organization->team()->first();
        // ddd([
        //     'team' => $team,
        //     'user' => $user,
        //     'owner' => $owner]
        // );
        $user->attachRole($owner, $team);
        return redirect()->route('organization.show', ['param' => $organization->slug]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function show(String $param)
    {
        $organization = Organization::where('id', $param)
            ->orWhere('slug', $param)
            ->with(['team', 'projects.campaigns'])
            ->firstOrFail();

        // $team = User::query()
        //     ->join('role_user', 'role_user.user_id', '=', 'users.id')
        //     ->join('teams', 'teams.id', '=', 'role_user.team_id')
        //     ->join('organizations', 'organizations.id', '=', 'teams.teamable_id')
        //     ->select('organizations.id', 'organizations.title', 'users.*')
        //     ->where('organizations.id', '=', $organization->id)
        //     ->distinct('users.id')
        //     ->get();

        return response()->view('organization.show', [
            'organization' => $organization
        ]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function edit(Organization $organization)
    {
        return response()->view('organization.edit', [
            'organization' => $organization
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Organization $organization)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function destroy(Organization $organization)
    {
        //
    }
}
