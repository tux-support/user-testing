<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(String $param)
    {
        DB::enableQueryLog();
        $user = User::where('id', $param)->firstOrFail();
        // $organizations = DB::table("SELECT `organizations`.`id`, `organizations`.`title`, `users`.`name`, `teams`.`teamable_type` FROM `role_user`
        //     INNER JOIN `users` on `users`.`id` = `role_user`.`user_id`
        //     INNER JOIN `teams` on `teams`.`id` = `role_user`.`team_id`
        //     INNER JOIN `organizations` on `organizations`.`id` = `teams`.`teamable_id` and `teams`.`teamable_type` LIKE '%Organization'
        //     WHERE `users`.`name` = 'Admin'")->get();


        // Use Laratrust to get teams
        // $organizations = DB::table('role_user')
        //     ->join('users', 'users.id', '=', 'role_user.user_id')
        //     ->join('teams', 'teams.id', '=', 'role_user.team_id')
        //     ->join('organizations', function ($join) {
        //         $join->on('organizations.id', '=', 'teams.teamable_id')
        //         ->where('teams.teamable_type', '=', 'App\Models\Organization');
        //     })
        //     ->select('organizations.id', 'organizations.title', 'users.name AS username')
        //     ->where('users.id', '=', $user->id)->get();

        //dd($organizations);

            // SELECT organizations.id, organizations.title, users.name, teams.teamable_type FROM role_user
            // INNER JOIN users on users.id = role_user.user_id
            // INNER JOIN teams on teams.id = role_user.team_id
            // INNER JOIN organizations on organizations.id = teams.teamable_id and teams.teamable_type LIKE '%Organization'
            // WHERE users.name = 'Admin'


        //$organizations = $user->rolesTeams->where('teamable_type', 'App\Models\Organization')->all();
        // $organizations = DB::table('organizations')
        //     ->leftJoin('role_user', 'user_id', '=', $user-id)

        //     $user->rolesTeams->where('teamable_type', 'App\Models\Organization')->all();
        // dd(DB::getQueryLog());
        return response()->view('profile.show', [
            'user' => $user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }
}
