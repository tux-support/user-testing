<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Team;

/**
 *
 */
trait Teamable
{


    public function team()
    {
        return $this->morphOne(Team::class, 'teamable');
    }

    // It is posisble to get multiple teams per teamable object
    public function teams()
    {
        return $this->morphMany(Team::class, 'teamable');
    }

    public function teamMembers()
    {
        return User::ByTeamableWithRolesCombined($this);
    }

    /**
     * Create default team or allow you to override values to create additional teams
     *
     * @param array $teamData
     * @return void
     */
    public function createTeam($teamData = [])
    {
        return Team::create(array_merge([
            'name' => Str::orderedUuid(),
            'display_name' => 'All members',
            'description' => 'All members of ' . $this->name,
            'teamable_id' => $this->id,
            'teamable_type' => $this->getMorphClass()
        ], $teamData));
    }


    public static function bootTeamable()
    {
        // This ensures that there is always at least one team associated with
        // any teamable object
        static::saved(function($model) {
            if(!$model->team) {
                //$model->createTeam();
            }
        });

        // Delete teams associated with this object, when it gets deleted
        static::deleted(function($model){
            //Team::destroy('id', $model->team->id);
        });
    }
}
