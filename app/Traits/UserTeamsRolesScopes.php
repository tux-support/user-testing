<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;


/**
 * This is used by the User model to scope queries related to teams
 */
trait UserTeamsRolesScopes
{

    /**
     * Explodes the comma separated roles into an array
     * Note that if you call $user->roles() you get the actual Laratrust roles of the user
     *
     * @param string $roles
     * @return array
     */
    public function getRolesAttribute(string $roles)
    {
        return explode(',', $roles);
    }

    public function scopeByTeam($query, $team)
    {
        return $query
        ->select('users.id', 'users.name', 'users.email', 'users.created_at', 'users.updated_at', 'role_user.team_id as team_id')
        ->join('role_user', 'role_user.user_id', '=', 'users.id')
        ->where('role_user.team_id', '=', $team->id);
        // ->distinct('users.id');
    }

    public function scopeByTeamable($query, $teamable)
    {
        return $query
            ->select('users.id', 'users.name', 'users.email', 'users.created_at', 'users.updated_at', 'teams.id as team_id', 'teams.name as team_name', 'teams.display_name as team_displayname')
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('teams', 'teams.id', '=', 'role_user.team_id')
            ->join($teamable->getTable(), $teamable->getTable() . '.id', '=', 'teams.teamable_id')
            ->where($teamable->getTable() . '.id', '=', $teamable->id)
            ->where('teams.teamable_type', '=', $teamable->getMorphClass());
            //->distinct('users.id');
    }

    public function scopeWithRoles($query)
    {
        return $query
            ->addSelect('roles.name as role_name', 'roles.display_name as role_display_name')
            ->join('roles', 'roles.id', '=', 'role_user.role_id');
    }


    public function scopeWithRolesCombined($query)
    {
        return $query
            ->addSelect(DB::raw("GROUP_CONCAT(roles.name) as roles"))
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->groupBy('users.id', 'team_id');
    }

}
