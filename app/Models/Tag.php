<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use \Spatie\Tags\HasTags;
use \Spatie\Tags\HasSlug;
use App\Models\Tagtype;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tag extends Model
{

    use HasTags, HasTranslations, HasSlug;
    use HasFactory;

    protected $tagtypes = true;

    public $translatable = ['name', 'slug', 'display_name', 'group_name', 'short_name'];

    public static function getTagClassName(): string
    {
        return Tag::class;
    }

    public function scopeWithType(Builder $query, string $type = null): Builder
    {
        if (is_null($type)) {
            return $query;
        }

        if($this->tagtypes) {
            return $query->join('tagtypes', 'tagtypes_id', '=', 'tagtypes.id')
                ->where('tagtypes.name', $type);
        } else {
            return $query->where('type', $type)->ordered();
        }
    }

    public function tagtype()
    {
        return $this->hasOne(Tagtype::class);
    }
}
