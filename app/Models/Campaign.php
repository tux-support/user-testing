<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\Teamable;
use \Spatie\Tags\HasTags;
use App\Models\Project;
use App\Models\User;

class Campaign extends Model
{
    use Sluggable;
    use HasTags;
    use Teamable;
    use HasFactory;

    protected $with = ['team'];
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'description', 'start_time',
        'end_time', 'test_start_time', 'test_end_time',
        'submission_start_time', 'submission_end_time'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];


    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function members()
    {
        return $this->hasMany(User::class);
    }

}
