<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Member extends User
{

    use \Parental\HasParent;
    use HasFactory;

    protected $fillable = [
        'name', 'email', 'password', 'roles'
    ];

    public static function boot()
    {
        parent::boot();

    }
}
