<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\Teamable;
use App\Models\Project;
use App\Models\User;
use App\Models\Team;
use App\Models\Membership;

class Organization extends Model
{
    use SoftDeletes;
    use Sluggable;
    use Teamable;
    use HasFactory;

    // Eager load team
    //protected $with = ['team'];

    protected $fillable = [
        'name', 'description'
    ];
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function projects()
    {
        return $this->hasMany(Project::class);
    }


    public function members()
    {
        return User::byTeamable($this);
    }

    /**
     * Get a list of memberships
     * Unlike the list of members, this list contains metadatam such as roles for each user
     *
     * @return void
     */
    public function memberships()
    {
        return $this->hasMany(Membership::class);
    }

    /**
     * Scope a query to only include popular users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    // public function scopeWithUser($query, $user)
    // {
    //     return $query->whereIn(
    //         'id', function($query, $user) {
    //             DB::table('role_user')
    //                 ->join('users', 'users.id', '=', 'role_user.user_id')
    //                 ->join('teams', 'teams.id', '=', 'role_user.team_id')
    //                 ->join('organizations', function ($join) {
    //                     $join->on('organizations.id', '=', 'teams.teamable_id')
    //                     ->where('teams.teamable_type', '=', 'App\Models\Organization');
    //                 })
    //                 ->select('organizations.id')
    //                 ->where('users.id', '=', $user->id);
    //         }
    //     );
    // }

    public function getMembershipsByRole($role)
    {
        return $this->memberships()->whereRaw("FIND_IN_SET('$role', `roles`)")->get();
    }


}
