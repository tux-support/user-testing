<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tagtype extends Model
{
    use  HasTranslations;
    use HasFactory;

    public $translatable = ['display_name', 'group_name'];

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
