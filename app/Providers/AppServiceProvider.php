<?php

namespace App\Providers;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Paginator::defaultView('vendor.pagination.bulma');
        Relation::morphMap([
            'users' => 'App\Models\User',
            'teams' => 'App\Models\Team',
            'organizations' => 'App\Models\Organization',
            'projects' => 'App\Models\Project',
            'campaigns' => 'App\Models\Campaign',
            'permissions' => 'App\Models\Permission',
            'roles' => 'App\Models\Role',
            'tags' => 'App\Models\Tag'
        ]);
    }
}
