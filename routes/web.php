<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\OrganizationController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\CampaignController;
use App\Http\Controllers\RoleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', [HomeController::class, 'index'])->name('home');

// Route::get('organization', [OrganizationController::class, 'index'])->name('organization.index');
// Route::get('organization/create', [OrganizationController::class, 'create'])->name('organization.create');
// Route::put('organization/store', [OrganizationController::class, 'store'])->name('organization.store');
// Route::get('organization/search', [OrganizationController::class, 'search'])->name('organization.search');
// Route::get('organization/show/{param}', [OrganizationController::class, 'show'])->name('organization.show');

// Route::get('profile', [ProfileController::class, 'index'])->name('profile.index');
// Route::get('profile/{param}', [ProfileController::class, 'show'])->name('profile.display');
// Route::get('profile/edit/{param}', [ProfileController::class, 'edit'])->name('profile.edit');

// //Route::get('team/{slug:name}', [TeamController::class, 'show'])->name('team.show');

// Route::resource('project', ProjectController::class)
// ->except(['show']);
// Route::get('project/{slug:name}', [ProjectController::class, 'show'])->name('project.show');

// Route::resource('campaign', CampaignController::class);
// Route::get('campaign/{slug:name}', [CampaignController::class, 'show'])->name('campaign.show');

// Route::view('about', 'about.index')->name('about');
// Route::view('credits', 'about.credits')->name('credits');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

//Auth::routes();
// Authentication Routes...
// $this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
// $this->post('login', 'Auth\LoginController@login');
// $this->post('logout', 'Auth\LoginController@logout')->name('logout');

// // Registration Routes...
// $this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
// $this->post('register', 'Auth\RegisterController@register');

// // Password Reset Routes...
// $this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
// $this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
// $this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
// $this->post('password/reset', 'Auth\ResetPasswordController@reset');




