<?php

namespace Database\Factories;


/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Organization;
use App\Models\User;
use App\Models\Member;
use App\Models\Team;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrganizationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Organization::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->sentence(rand(3,6));

        return [
            'name' => $name,
            'slug' => Str::slug($name),
            'description' => $this->faker->paragraph(3),
        ];
    }

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterMaking(function (Organization $organization) {
            //
        })->afterCreating(function (Organization $organization) {
            // $team = Team::make();
            // $team->display_name = 'All members';
            // $team->description = 'All members of ' . $organization->name;
            // $team->teamable_id = $organization->id;
            // $team->teamable_type = (new \ReflectionClass($organization))->getName();
            // $team->save();


            // $organization->team_id = $team->id;
            // $users = User::inRandomOrder()->limit(rand(3,10));
            // $organization->members()->saveMany($users);
            // $roles = Role::all()->random(rand(1,3));
            // $user->attachRoles($roles, $organization->team);
        });
    }
};

