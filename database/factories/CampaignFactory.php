<?php

namespace Database\Factories;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Campaign;
use Illuminate\Support\Str;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\Factory;

class CampaignFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Campaign::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $title = $this->faker->sentence(rand(2,6)),
    //        'slug' => Str::slug($title),
            'description' => $this->faker->paragraph(3),
            'start_time' => $startTime = $this->faker->dateTimeBetween('now', '+30 days'),
            'end_time' => $endTime = $this->faker->dateTimeBetween($startTime, '+60 days'),
            'test_start_time' => $testEndTime = $this->faker->dateTimeBetween($startTime, $endTime),
            'test_end_time' => $testEndTime = $this->faker->dateTimeBetween($startTime, $endTime),
            'submission_start_time' => $submissionStartTime = $this->faker->dateTimeBetween($startTime, $testEndTime),
            'submission_end_time' => $this->faker->dateTimeBetween($submissionStartTime, $testEndTime)
        ];
    }
};
