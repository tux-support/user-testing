<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ExtendTagsInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('tagtypes', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name')->unique();
            $table->json('display_name');
        });

        if (!Schema::hasColumn('tags', 'display_name')
            && !Schema::hasColumn('tags', 'display_name')) {
            Schema::table('tags', function (Blueprint $table) {
                $table->json('display_name')->after('type');
                $table->json('short_name')->after('display_name');
                $table->json('group_name')->after('short_name');

                $table->foreignId('tagtypes_id');
                //$table->unique(['display_name', 'group_name']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('tags', 'display_name')
            && !Schema::hasColumn('tags', 'display_name')) {
                Schema::table('tags', function (Blueprint $table) {
                    $table->dropColumn(['display_name', 'group_name', 'short_name']);
                });
        }

        Schema::drop('tagtypes');
    }
}
