<?php

namespace Database\Seeders;

use Laratrust\Models\LaratrustRole;
use Laratrust\Models\LaratrustPermission;
use Laratrust\Models\LaratrustTeam;

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Permission;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rolesDataList = [
            'Guest' => [
                'display_name' => 'Guest',
                'description' => 'A user with access to read public content',
                'permissions' => []
            ],
            'Applicant' => [
                'display_name' => 'Applicant',
                'description' => 'A user with a pending application to become a member',
                'permissions' => []
            ],
            'Member' => [
                'display_name' => 'Member',
                'description' => 'A regular member that can read internal messages',
                'permissions' => [
                    'read-posts'
                ],
            ],
            'Analyst' => [
                'display_name' => 'Analyst',
                'description' => 'Can retrieve reports and statistics',
                'permissions' => [
                    'read-reports'
                ],
            ],
            'Content Moderator' => [
                'display_name' => 'Content Moderator',
                'description' => 'Can perform administrative tasks on content',
                'permissions' => [
                    'delete-posts',
                    'restore-posts',
                    'publish-posts',
                    'unpublish-posts',
                    'edit-posts'
                ],
            ],
            'User Moderator' => [
                'display_name' => 'User moderator',
                'description' => 'Has privileges to perform administrative tasks on users',
                'permissions' => [
                    'accept-user',
                    'reject-user',
                    'remove-user',
                    'timeout-user'
                ]
            ],
            'User admin' => [
                'display_name' => 'User administrator',
                'description' => 'Can promote and demote user moderators',
                'permissions' => [
                    'promote-user-moderator',
                    'demote-user-moderator'
                ]
            ],
            'Organization admin' => [
                'display_name' => 'Organization administrator',
                'description' => 'Can update organization info and add projects and campaigns',
                'permissions' => [
                    'edit-organization',
                    'edit-projects',
                    'edit-campaigns'
                ]
            ],
            'Organization owner' => [
                'display_name' => 'Organization owner',
                'description' => 'Can promote and demote organization admins and can remove an organization',
                'permissions' => [
                    'promote-org-admin',
                    'demote-org-admin',
                    'remove-org'
                ]
            ]
        ];

        $permissionDataList = [
            'read-posts' => [
                'display_name' => 'Reads posts',
                'description' => 'Read any internal posts'
            ],
            'generate-reports' => [
                'display_name' => 'Generate reports',
                'description' => 'Generate new reports'
            ],
            'read-reports' => [
                'display_name' => 'Read reports',
                'description' => 'Read any existing reports'
            ],
            'delete-posts' => [
                'display_name' => 'Delete posts',
                'description' => 'Delete any existing posts'
            ],
            'restore-posts' => [
                'display_name' => 'Restore posts',
                'description' => 'Restore any posts from deleted state'
            ],
            'publish-posts' => [
                'display_name' => 'Publish posts',
                'description' => 'Publish any unpuplished posts'
            ],
            'unpublish-posts' => [
                'display_name' => 'Unpublish posts',
                'description' => 'Unpublish any published posts'
            ],
            'edit-posts' => [
                'display_name' => 'Edit posts',
                'description' => 'Edit any posts'
            ],
            'accept-user' => [
                'display_name' => 'Accept user',
                'description' => 'Accept a user application'
            ],
            'reject-user' => [
                'display_name' => 'Reject user',
                'description' => 'Reject a user application'
            ],
            'remove-user' => [
                'display_name' => 'Remove user',
                'description' => 'Remove user'
            ],
            'timeout-user' => [
                'display_name' => 'Timeout user',
                'description' => 'Send a user to timeout'
            ],
            'promote-user-moderator' => [
                'display_name' => 'Promote user to moderator',
                'description' => 'Promote a user to moderator status'
            ],
            'demote-user-moderator' => [
                'display_name' => 'Demote moderator',
                'description' => 'Demote a moderator to regular user status'
            ],
            'edit-organization' => [
                'display_name' => 'Edit organization',
                'description' => 'Edit organization information'
            ],
            'edit-projects' => [
                'display_name' => 'Edit projects',
                'description' => 'Edit project information'
            ],
            'create-campaigns' => [
                'display_name' => 'Create new campaigns',
                'description' => 'Create new campaigns'
            ],
            'edit-campaigns' => [
                'display_name' => 'Edit campaigns',
                'description' => 'Edit existing campaigns'
            ],
            'remove-campaigns' => [
                'display_name' => 'Remove existing campaigns',
                'description' => 'Remove existing campaigns'
            ],
            'promote-org-admin' => [
                'display_name' => 'Promote organization admin',
                'description' => 'Promote a user to organization administrator'
            ],
            'demote-org-admin' => [
                'display_name' => 'Demote organization admin',
                'description' => 'Demote an existing organization administrator'
            ],
            'remove-org' => [
                'display_name' => 'Remove organization',
                'description' => 'Permanently remove organization'
            ]
        ];

        $permissions = [];
        foreach($permissionDataList as $permissionKey => $permissionData) {
            $permissions[$permissionKey] = Permission::create(array_merge(['name' => $permissionKey],$permissionData));
        }

        $roles = [];
        foreach($rolesDataList as $roleKey => $roleData)
        {
            $roles[$roleKey] = Role::create([
                'name' => $roleKey,
                'display_name' => array_key_exists('display_name', $roleData)?$roleData['display_name']:'',
                'description' => array_key_exists('description', $roleData)?$roleData['description']:''
            ]);
            if($roleData['permissions']) {
                 $roles[$roleKey]->attachPermissions($roleData['permissions']);
            }
        }


    }
}
