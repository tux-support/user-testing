<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        return;
        $this->call(RolesSeeder::class);
        if (App::environment('local')) {
            $this->call(AdminSeeder::class);
        }
        $this->call(TagSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(OrganizationSeeder::class);
        //$this->call(OrganizationMemberSeeder::class);
        $this->call(ProjectSeeder::class);
        //$this->call(ProjectMemberSeeder::class);
        $this->call(CampaignSeeder::class);
        //$this->call(CampaignMemberSeeder::class);
    }
}
