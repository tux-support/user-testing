<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Organization;
use App\Models\Role;
use App\Models\User;
use App\Models\Team;

class OrganizationMemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(Organization::with('team')->get() as $organization) {
            //echo $organization->id ." : " . $organization->title . "(" . $organization->team . ")". PHP_EOL;
            foreach(User::inRandomOrder()->limit(rand(3,15))->get() as $user) {
                //echo $user->name . PHP_EOL;
                $roles = Role::inRandomOrder()->limit(rand(1,5))->get();
                $user->attachRoles($roles->all(), $organization->team);
            }
            //echo PHP_EOL;
        }
    }
}
