<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Str;
use App\Models\User;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Admin',
            'email' => 'admin@usertest',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);
        // $user->attachRole('Member');
        // $user->attachRole('Content Moderator');
        // $user->attachRole('User moderator');
        // $user->attachRole('User admin');
        // $user->attachRole('Organization Owner');

        //$user->syncRoles(['Member', 'Editor', 'Content Moderator', 'User Admin', 'Organization Owner']);


    }
}
