<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create([
            'name' => 'view articles',
            'display_name' => 'View articles',
            'description' => 'View articles'
        ]);
        Permission::create([
            'name' => 'create articles',
            'display_name' => 'Create articles',
            'description' => 'Create articles'
        ]);
        Permission::create([
            'name' => 'edit articles',
            'display_name' => 'Edit articles',
            'description' => 'Edit articles'
        ]);
        Permission::create([
            'name' => 'delete articles',
            'display_name' => 'Delete articles',
            'description' => ''
        ]);
        Permission::create([
            'name' => 'publish articles',
            'display_name' => 'Publish articles',
            'description' => ''
        ]);
        Permission::create([
            'name' => 'unpublish articles',
            'display_name' => 'Unpublish articles',
            'description' => ''
        ]);
        Permission::create([
            'name' => 'edit own articles',
            'display_name' => 'Edit own articles',
            'description' => ''
        ]);
        Permission::create([
            'name' => 'unpublish own articles',
            'display_name' => 'Unpublish own articles',
            'description' => ''
        ]);
        Permission::create([
            'name' => 'delete own articles',
            'display_name' => 'Delete own articles',
            'description' => ''
        ]);
        Permission::create([
            'name' => 'invite users',
            'display_name' => 'Invite users',
            'description' => ''
        ]);
        Permission::create([
            'name' => 'approve users',
            'display_name' => 'Approve users',
            'description' => ''
        ]);
        Permission::create([
            'name' => 'register users',
            'display_name' => 'Register users',
            'description' => ''
        ]);
        Permission::create([
            'name' => 'timeout users',
            'display_name' => 'Timeout users',
            'description' => ''
        ]);
        Permission::create([
            'name' => 'ban users',
            'display_name' => 'Ban users',
            'description' => ''
        ]);
        Permission::create([
            'name' => 'assign roles',
            'display_name' => 'Assign roles',
            'description' => ''
        ]);
        Permission::create([
            'name' => 'assign admin roles',
            'display_name' => 'Assign admin roles',
            'description' => ''
        ]);
        Permission::create([
            'name' => 'assign permissions',
            'display_name' => 'Assign permissions',
            'description' => ''
        ]);

        $user = Role::create([
            'name' => 'guest',
            'display_name' => 'Guest',
            'description' => 'Can view public posts'
        ]);
        $user->givePermissionTo('view articles');

        $user = Role::create([
            'name' => 'member',
            'display_name' => 'Member',
            'description' => 'Regular member. Can create content and invite users'
        ]);
        $user->givePermissionTo(['view articles', 'create articles', 'edit articles', 'invite users']);

        $user = Role::create([
            'name' => 'editor',
            'display_name' => 'Editor',
            'description' => 'Can publish and unpublish content'
        ]);
        $user->givePermissionTo(['publish articles', 'unpublish articles']);

        $user = Role::create([
            'name' => 'moderator',
            'display_name' => 'Moderator',
            'description' => 'Can timeout and approve users'
        ]);
        $user->givePermissionTo(['timeout users', 'unpublish articles', 'approve users']);

        $user = Role::create([
            'name' => 'admin',
            'display_name' => 'Administrator',
            'description' => 'Can ban and register users as well as edit their roles'
        ]);
        $user->givePermissionTo(['ban users', 'register users', 'assign roles']);

        $user = Role::create([
            'name' => 'superadmin',
            'display_name' => 'Super admin',
            'description' => 'Can assign other admins and permissions'
        ]);
        $user->givePermissionTo(['assign admin roles', 'assign permissions']);

        $user = Role::create([
            'name' => 'owner',
            'display_name' => 'Owner',
            'description' => 'Can edit own content'
        ]);
        $user->givePermissionTo(['edit own articles', 'delete own articles', 'unpublish own articles']);
    }
}
