<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Tagtype;
use App\Models\Tag;
use Illuminate\Support\Str;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $campaigntags = [
            'Device Type' => [
                'tag-device-desktop' => [
                    'display_name' => 'Desktop',
                ],
                'tag-device-mobile' => [
                    'display_name' => 'Mobile',
                ],
                'tag-device-os' => [
                    'display_name' => 'Tablet',
                ],
                'tag-device-website' => [
                    'display_name' => 'Virtual Reality',
                    'short_name' => 'VR'

                ]
            ],
            'Application Category' => [
                'Operating System' => [
                    'display_name' => 'Operating System',
                    'short_name' => 'OS'
                ],
                'Office' => [
                    'display_name' => 'Office',
                ],
                'Graphics' => [
                    'display_name' => 'Graphics',
                    'short_name' => 'Gfx'
                ],
                'Audio' => [
                    'display_name' => 'Audio',
                ],
                'Network' => [
                    'display_name' => 'Network',
                    'short_name' => 'Net'
                ],
                'System Utility' => [
                    'display_name' => 'System Utility',
                    'short_name' => 'Sysutils'
                ],
                'Tools' => [
                    'display_name' => 'Tools',
                ],
                'Games' => [
                    'display_name' => 'Games',
                ],
                'Education' => [
                    'display_name' => 'Education',
                    'short_name' => 'Edu'
                ],
                'Development' => [
                    'display_name' => 'Development',
                    'short_name' => 'Dev'

                ],
            ],
            'Target Skill Level' => [
                'Beginners' => [
                    'display_name' => 'Beginners',
                ],
                'Intermediate' => [
                    'display_name' => 'Intermediate',
                ],
                'Advanced' => [
                    'display_name' => 'Advanced',
                ],
                'Expert' => [
                    'display_name' => 'Expert',
                ],
            ],
            'User Interface' => [
                'Web based' => [
                    'display_name' => 'Web Based',
                    'short_name' => 'Web'
                ],
                'Graphical Client' => [
                    'display_name' => 'Graphical',
                    'short_name' => 'GUI'
                ],
                'Command Line' => [
                    'display_name' => 'Command Line',
                    'short_name' => 'Cmd'
                ],
                'Terminal UI' => [
                    'display_name' => 'Terminal UI',
                    'short_name' => 'TUI'
                ],
            ],
            'UI Toolkit' => [
                'GTK 2' => [
                    'display_name' => 'GTK 2',
                    'short_name' => 'GTK2',
                ],
                'GTK 3' => [
                    'display_name' => 'GTK 3',
                    'short_name' => 'GTK3',
                ],
                'KDE Plasma 5' => [
                    'display_name' => 'Plasma 5',
                ],
                'KDE Kirigami (Plasma Mobile)' => [
                    'display_name' => 'Kirigami',
                ],
                'Qt 4' => [
                    'display_name' => 'Qt4',
                ],
                'Qt 5' => [
                    'display_name' => 'Qt5',
                ],
                'WxWidgets' => [
                    'display_name' => 'WxWidgets',
                ],
                'Microsoft .Net' => [
                    'display_name' => 'Microsoft .Net/Mono',
                    'short_name' => '.net',
                ],
                'Electron' => [
                    'display_name' => 'Electron',
                ],
            ],
            'OS Compatibility' => [
                'Linux' => [
                    'display_name' => 'GNU/Linux',
                    'short_name' => 'Linux',
                ],
                'BSD' => [
                    'display_name' => 'BSD',
                ],
                'Microsoft Windows' => [
                    'display_name' => 'Windows',
                ],
                'Apple iOS' => [
                    'display_name' => 'Apple iOS',
                    'short_name' => 'iOS',
                ],
                'Apple macOS' => [
                    'display_name' => 'Apple macOS',
                    'short_name' => 'macOS',
                ],
            ],
            'User Input Support' => [
                'Mouse' => [
                    'display_name' => 'Mouse',
                ],
                'Keyboard' => [
                    'display_name' => 'Keyboard',
                ],
                'Touch' => [
                    'display_name' => 'Touch',
                ],
                'Voice' => [
                    'display_name' => 'Voice',
                ],
                'Gesture' => [
                    'display_name' => 'Gesture',
                ],
            ],
        ];
        foreach($campaigntags as $groupname => $taggroup) {
            $tagtype = Tagtype::create([
                'name' => $groupname,
                'display_name' => $groupname,
            ]);
            foreach($taggroup as $group => $tags) {
                Tag::create([
                    'type' => $groupname,
                    'name' => $tags['display_name'],
                    'display_name' => $tags['display_name'],
                    'short_name' => array_key_exists('short_name', $tags)?$tags['short_name']:substr($tags['display_name'], 0, 15),
                    'group_name' => $groupname,
                    'tagtypes_id' => $tagtype->id,
                    'slug' => $tags['display_name'],
                ]);
            }
        }
    }
}
