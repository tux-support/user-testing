<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Project;
use App\Models\Campaign;

class CampaignSeeder extends Seeder
{

    protected $campaigns = [
        [
            'cssclasses' => 'mix color-1 check1 radio3 option2',
            'tags' => ['desktop', 'developer-run', 'open-enrollment'],
            'title' => 'BDDL Distro Test 225',
            'owner' => [
                'logo' => 'img/logos/Ubuntu-Logo.png',
                'name' => 'Ubuntu Desktop Team',
                'url' => 'https://ubuntu.org'
            ],
            'description' => 'This week we\'re testing Lubuntu. Come join us on saturday nights!',
            'image' => 'img/logos/Ubuntu-Logo.png'
        ],[
            'cssclasses' => 'mix color-2 check2 radio3 option3',
            'tags' => ['desktop', 'developer-run', 'open-enrollment'],
            'title' => 'Desktop UI testing',
            'owner' => [
                'logo' => 'img/logos/Ubuntu-Logo.png',
                'name' => 'Ubuntu Desktop Team',
                'url' => 'https://ubuntu.org'
            ],
            'description' => 'Ubuntu is testing a brand new desktop environment build on Flutter. And YOU can be a part of it.',
            'image' => 'img/logos/Ubuntu-Logo.png'
        ],[
            'cssclasses' => 'mix color-2 check2 radio2 option1',
            'tags' => ['desktop', 'developer-run', 'open-enrollment'],
            'title' => 'Desktop UI testing',
            'owner' => [
                'logo' => 'img/logos/Ubuntu-Logo.png',
                'name' => 'Ubuntu Desktop Team',
                'url' => 'https://ubuntu.org'
            ],
            'description' => 'Ubuntu is testing a brand new desktop environment build on Flutter. And YOU can be a part of it.',
            'image' => 'img/logos/Ubuntu-Logo.png'
        ],[
            'cssclasses' => 'mix color-1 check1 radio3 option4',
            'tags' => ['desktop', 'developer-run', 'open-enrollment'],
            'title' => 'Desktop UI testing',
            'owner' => [
                'logo' => 'img/logos/Ubuntu-Logo.png',
                'name' => 'Ubuntu Desktop Team',
                'url' => 'https://ubuntu.org'
            ],
            'description' => 'Ubuntu is testing a brand new desktop environment build on Flutter. And YOU can be a part of it.',
            'image' => 'img/logos/Ubuntu-Logo.png'
        ],[
            'cssclasses' => 'mix color-2 check1 radio2 option3',
            'tags' => ['desktop', 'rewards', 'open-enrollment'],
            'title' => 'Desktop UI testing',
            'owner' => [
                'logo' => 'img/logos/Ubuntu-Logo.png',
                'name' => 'Ubuntu Desktop Team',
                'url' => 'https://ubuntu.org'
            ],
            'description' => 'Ubuntu is testing a brand new desktop environment build on Flutter. And YOU can be a part of it.',
            'image' => 'img/logos/Ubuntu-Logo.png'
        ],[
            'cssclasses' => 'mix color-1 check3 radio2 option4',
            'tags' => ['desktop', 'developer-run', 'open-enrollment'],
            'title' => 'Desktop UI testing',
            'owner' => [
                'logo' => 'img/logos/Ubuntu-Logo.png',
                'name' => 'Ubuntu Desktop Team',
                'url' => 'https://ubuntu.org'
            ],
            'description' => 'Ubuntu is testing a brand new desktop environment build on Flutter. And YOU can be a part of it.',
            'image' => 'img/logos/Ubuntu-Logo.png'
        ],[
            'cssclasses' => 'mix color-1 check3 radio3 option2',
            'tags' => ['desktop', 'developer-run', 'rewards'],
            'title' => 'Desktop UI testing',
            'owner' => [
                'logo' => 'img/logos/Ubuntu-Logo.png',
                'name' => 'Ubuntu Desktop Team',
                'url' => 'https://ubuntu.org'
            ],
            'description' => 'Ubuntu is testing a brand new desktop environment build on Flutter. And YOU can be a part of it.',
            'image' => 'img/logos/Ubuntu-Logo.png'
        ],[
            'cssclasses' => 'mix color-1 check3 radio3 option2',
            'tags' => ['desktop', 'developer-run', 'open-enrollment'],
            'title' => 'Desktop UI testing',
            'owner' => [
                'logo' => 'img/logos/Ubuntu-Logo.png',
                'name' => 'Ubuntu Desktop Team',
                'url' => 'https://ubuntu.org'
            ],
            'description' => 'Ubuntu is testing a brand new desktop environment build on Flutter. And YOU can be a part of it.',
            'image' => 'img/logos/Ubuntu-Logo.png'
        ],[
            'cssclasses' => 'mix color-1 check3 radio2 option4',
            'tags' => ['app', 'community-run', 'invite-only'],
            'title' => 'Desktop UI testing',
            'owner' => [
                'logo' => 'img/logos/Ubuntu-Logo.png',
                'name' => 'Ubuntu Desktop Team',
                'url' => 'https://ubuntu.org'
            ],
            'description' => 'Ubuntu is testing a brand new desktop environment build on Flutter. And YOU can be a part of it.',
            'image' => 'img/logos/Ubuntu-Logo.png'
        ],[
            'cssclasses' => 'mix color-1 check3 radio3 option1',
            'tags' => ['desktop', 'developer-run', 'open-enrollment'],
            'title' => 'Desktop UI testing',
            'owner' => [
                'logo' => 'img/logos/Ubuntu-Logo.png',
                'name' => 'Ubuntu Desktop Team',
                'url' => 'https://ubuntu.org'
            ],
            'description' => 'Ubuntu is testing a brand new desktop environment build on Flutter. And YOU can be a part of it.',
            'image' => 'img/logos/Ubuntu-Logo.png'
        ],[
            'cssclasses' => 'mix color-2 check2 radio2 option2',
            'tags' => ['desktop', 'developer-run', 'open-enrollment'],
            'title' => 'Desktop UI testing',
            'owner' => [
                'logo' => 'img/logos/Ubuntu-Logo.png',
                'name' => 'Ubuntu Desktop Team',
                'url' => 'https://ubuntu.org'
            ],
            'description' => 'Ubuntu is testing a brand new desktop environment build on Flutter. And YOU can be a part of it.',
            'image' => 'img/logos/Ubuntu-Logo.png'
        ],[
            'cssclasses' => 'mix color-1 check1 radio2 option3',
            'tags' => ['desktop', 'community-run', 'open-enrollment'],
            'title' => 'Desktop UI testing',
            'owner' => [
                'logo' => 'img/logos/Ubuntu-Logo.png',
                'name' => 'Ubuntu Desktop Team',
                'url' => 'https://ubuntu.org'
            ],
            'description' => 'Ubuntu is testing a brand new desktop environment build on Flutter. And YOU can be a part of it.',
            'image' => 'img/logos/Ubuntu-Logo.png'
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $projects = Project::all();
        foreach($projects as $project) {
            Campaign::factory()->times(rand(1,4))->create(['project_id' => $project->id]);
        }
    }
}
