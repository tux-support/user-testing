<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Project;
use App\Models\Organization;

class ProjectSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $organizations = Organization::all();

        foreach($organizations as $organization) {
            Project::factory()->times(rand(1,10))->create(['organization_id' => $organization->id]);
//            factory(Member::class, rand(1,10))->create(['organization_id' => $organization->id]);
        }
    }
}
