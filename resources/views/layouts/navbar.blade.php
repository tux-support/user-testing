<style>
    .navbar-item .icon, .navbar-item .icon:only-child, .navbar-item {
        margin-right: 1ex;
    }
</style>
<nav class="navbar has-shadow is-primary" role="navigation"
    aria-label="main navigation">
    <div class="navbar-brand">
        <a href="{{ url('/') }}"
            class="navbar-item">Shoutout!</a>
        </a>

        <a role="button" class="navbar-burger burger" aria-label="menu"
            aria-expanded="false" data-target="navbarBasicExample">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
        </a>
    </div>

    <div id="top-navbar" class="navbar-menu">
        <div class="navbar-start">
            <a class="navbar-item @if (Route::current()->getName() == 'home') is-active @endif"
                href="{{ route('home') }}">
                <span class="icon is-medium">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="white ">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6" />
                  </svg>
                  </span>
                Home
            </a>

            <a class="navbar-item @if (Route::current()->getName() == 'campaign.index') is-active @endif"
                href="{{ route('campaign.index') }}">
                <span class="icon is-medium">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="white">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M11 5.882V19.24a1.76 1.76 0 01-3.417.592l-2.147-6.15M18 13a3 3 0 100-6M5.436 13.683A4.001 4.001 0 017 6h1.832c4.1 0 7.625-1.234 9.168-3v14c-1.543-1.766-5.067-3-9.168-3H7a3.988 3.988 0 01-1.564-.317z" />
                      </svg>
                  </span>

                Campaigns
            </a>

            <a class="navbar-item @if (stripos(Route::current()->getName(), 'organization') === 0) is-active @endif"
                href="{{ route('organization.index') }}">
                <span class="icon is-medium">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="white">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M19 21V5a2 2 0 00-2-2H7a2 2 0 00-2 2v16m14 0h2m-2 0h-5m-9 0H3m2 0h5M9 7h1m-1 4h1m4-4h1m-1 4h1m-5 10v-5a1 1 0 011-1h2a1 1 0 011 1v5m-4 0h4" />
                      </svg>
                  </span>
                Organizations
            </a>

            <div class="navbar-item has-dropdown is-hoverable">
                <a class="navbar-link">
                    More
                </a>

                <div class="navbar-dropdown">
                    <a class="navbar-item" href="{{ route('about') }}">
                        About
                    </a>
                    <a class="navbar-item" href="{{ route('credits') }}">
                        Credits
                    </a>
                </div>
            </div>
        </div>

        <div class="navbar-end">
            @if(Auth::guest())
                <a class="navbar-item "
                    href="{{ route('login') }}">Login</a>
                <a class="navbar-item "
                    href="{{ route('register') }}">Register</a>
            @else
                <div class="navbar-item has-drowndown is-hoverable">
                    <a class="navbar-link"
                        href="#">Create</a>
                        <div class="navbar-dropdown">
                            <a class="navbar-item"
                                href="{{ route('organization.create') }}">
                                Organization
                            </a>

                        </div>
                </div>
                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link"
                        href="#">{{ Auth::user()->name }}</a>

                    <div class="navbar-dropdown">
                        <a class="navbar-item"
                            href="{{ route('logout') }}"
                            onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form"
                            action="{{ route('logout') }}"
                            method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            @endif
        </div>
    </div>
</nav>
