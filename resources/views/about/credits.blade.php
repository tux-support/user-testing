@extends('layouts.app')

@section('breadcrumbs')
<li class="is-active"><a href="{{ Route::current()->uri() }}" aria-current="page">Credits</a></li>
@endsection

@section('content')
<style>
    a.card-footer-item {
        text-align: center;
    }

    a.card-footer-item:hover {
        background-color: lightgray;
        cursor: pointer;
    }

    .icon-mask {
        mask: url({{ asset('img/icons/All_rights_reserved_logo.svg') }});
        display: inline-block;
        line-height: 1;
        flex-shrink: 0;
        mask-size: contain;
        background-color: hsl(217, 71%, 53%);
    }
    .card-fullheight {
        display: flex;
        justify-content: space-between;
        height: 100%;
        flex-direction: column;
    }
    .card-fullheight > * {

    }

    .card-fullheight .card-footer, .card-fullheight .card-footer .card-footer-item {
    }
 </style>
<div id="credits" class="box" style="min-height: 100%;">
    <h1 class="title is-1">Credits</h1>
    <div class="columns flex-wrap is-multiline">

        <div class="column is-4-desktop is-3-widescreen is-half-tablet">
            <div class="card card-fullheight">
                <div class="card-header">
                    <p class="card-header-title is-centered">
                        <a href="https://bulma.io/">Bulma CSS framework</a>
                    </p>
                </div>
                <div class="card-image">
                    <figure class="image is-4by1">
                        <img src="{{ asset('img/logos/bulma-logo.png') }}" alt="Megaphone, people and a computer">
                    </figure>
                </div>
                <div class="card-content">
                    Simple CSS framework without any requirement on Javascript
                </div>
                <footer class="card-footer">
                    <a href="https://bulma.io/" class="card-footer-item has-text-centered">
                        <span class="icon is-medium">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14" />
                            </svg>
                        </span>
                        Website
                        </a>
                      <a href="https://opensource.org/licenses/mit-license.php" class="card-footer-item">
                        <span class="icon is-medium">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M3 6l3 1m0 0l-3 9a5.002 5.002 0 006.001 0M6 7l3 9M6 7l6-2m6 2l3-1m-3 1l-3 9a5.002 5.002 0 006.001 0M18 7l3 9m-3-9l-6-2m0-2v2m0 16V5m0 16H9m3 0h3" />
                              </svg>
                        </span>
                          MIT
                        </a>
                  </footer>
            </div>
        </div>

        <div class="column is-4-desktop is-3-widescreen is-half-tablet">
            <div class="card card-fullheight">
                <div class="card-header">
                    <p class="card-header-title is-centered">
                        <a href="https://www.csrhymes.com/bulma-block-list/">Bulma Block List</a>
                    </p>
                </div>
                <div class="card-image">
                    <figure class="image is-4by3">
                        <img src="{{ asset('img/logos/bulma-block-list.png') }}" alt="Megaphone, people and a computer">
                    </figure>
                </div>
                <div class="card-content">
                    Very nice blocky lists
                </div>
                <footer class="card-footer">
                    <a href="https://www.csrhymes.com/bulma-block-list/" class="card-footer-item has-text-centered">
                        <span class="icon is-medium">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14" />
                            </svg>
                        </span>
                        Website
                        </a>
                      <a href="https://opensource.org/licenses/mit-license.php" class="card-footer-item">
                        <span class="icon is-medium">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M3 6l3 1m0 0l-3 9a5.002 5.002 0 006.001 0M6 7l3 9M6 7l6-2m6 2l3-1m-3 1l-3 9a5.002 5.002 0 006.001 0M18 7l3 9m-3-9l-6-2m0-2v2m0 16V5m0 16H9m3 0h3" />
                              </svg>
                        </span>
                          MIT
                        </a>
                  </footer>
            </div>
        </div>
        <div class="column is-4-desktop is-3-widescreen is-half-tablet">
            <div class="card card-fullheight">
                <div class="card-header">
                    <p class="card-header-title is-centered">
                        <a href="https://heroicons.com/">heroicons</a>
                    </p>
                </div>
                <div class="card-image">
                    <figure class="image is-1by4">
                        <img src="{{ asset('img/logos/heroicon-logo.png') }}" alt="heroicons logo">
                    </figure>
                </div>
                <div class="card-content">
                    Beautiful hand-crafted SVG icons, by the makers of Tailwind CSS
                </div>
                <footer class="card-footer">
                    <a href="https://www.csrhymes.com/bulma-block-list/" class="card-footer-item has-text-centered">
                        <span class="icon is-medium">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14" />
                            </svg>
                        </span>
                        Website
                        </a>
                      <a href="https://opensource.org/licenses/mit-license.php" class="card-footer-item">
                        <span class="icon is-medium">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M3 6l3 1m0 0l-3 9a5.002 5.002 0 006.001 0M6 7l3 9M6 7l6-2m6 2l3-1m-3 1l-3 9a5.002 5.002 0 006.001 0M18 7l3 9m-3-9l-6-2m0-2v2m0 16V5m0 16H9m3 0h3" />
                              </svg>
                        </span>
                          MIT
                        </a>
                  </footer>
            </div>
        </div>

        <div class="column is-4-desktop is-3-widescreen is-half-tablet">
            <div class="card card-fullheight">
                <div class="card-header">
                    <p class="card-header-title is-centered">
                        <a href="https://www.freepik.com/free-vector/noisy-big-megaphone_9650130.htm">Shoutie image</a>
                    </p>
                </div>
                <div class="card-image">
                    <figure class="image is-4by3">
                        <img src="{{ asset('img/shout-300w.jpg') }}" alt="Megaphone, people and a computer">
                    </figure>
                </div>

                <div class="card-content">
                    Noisy big megaphone Free Vector
                    <a href="http://www.freepik.com">Designed by pch.vector / Freepik</a>
                </div>
                <footer class="card-footer">
                    <a href="http://www.freepik.com" class="card-footer-item has-text-centered">
                        <span class="icon is-medium">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14" />
                            </svg>
                        </span>
                        Website
                        </a>

                      <a href="https://opensource.org/licenses/mit-license.php" class="card-footer-item">
                        <span class="icon is-medium icon-mask has-text-link">
                        </span>
                          Freepik free license
                        </a>
                  </footer>
            </div>
        </div>
        <div class="column is-4-desktop is-3-widescreen is-half-tablet">
            <div class="card card-fullheight">
                <div class="card-header">
                    <p class="card-header-title is-centered">
                        <a href="https://laravel.com/">Laravel</a>
                    </p>
                </div>                <div class="card-image">
                    <figure class="image is-square">
                        <img src="{{ asset('img/logos/laravel-logo.jpg') }}" alt="Megaphone, people and a computer">
                    </figure>
                </div>
                <div class="card-content">
                    <p class="content">A PHP framework for web artisans</p>
                    <p class="content">Laravel is an amazing PHP framework that makes it easy to build fast and secure web pages</p>
                </div>
                <footer class="card-footer">
                    <a href="https://www.csrhymes.com/bulma-block-list/" class="card-footer-item has-text-centered">
                        <span class="icon is-medium">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14" />
                            </svg>
                        </span>
                        Website
                        </a>
                      <a href="https://laravel.com/" class="card-footer-item">
                        <span class="icon is-medium">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M3 6l3 1m0 0l-3 9a5.002 5.002 0 006.001 0M6 7l3 9M6 7l6-2m6 2l3-1m-3 1l-3 9a5.002 5.002 0 006.001 0M18 7l3 9m-3-9l-6-2m0-2v2m0 16V5m0 16H9m3 0h3" />
                              </svg>
                        </span>
                          MIT
                        </a>
                  </footer>
            </div>
        </div>

    </div>
</div>

@endsection
