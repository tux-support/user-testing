@extends('layouts.app')

@section('breadcrumbs')
@endsection

@section('content')

        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/dashboard') }}" class="button">Dashboard</a>
                    @else
                        <a href="{{ url('/login') }}" class="button">Login</a>
                        <a href="{{ url('/register') }}" class="button">Register</a>
                    @endif
                </div>
            @endif
        </div>
@endsection
