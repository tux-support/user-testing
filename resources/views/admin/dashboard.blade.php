@extends('layouts.app')

@section('breadcrumbs')
    <li class="is-active"><a href="{{ Route::current()->uri() }}" aria-current="page">Dashboard</a></li>
@endsection

@section('content')


        <nav class="panel">
            <p class="panel-heading">
                <h1>Admin dashboard!</h1>
            </p>
            <div class="panel-block">
              <p class="control has-icons-left">
                <input class="input" type="text" placeholder="Search">
                <span class="icon is-left">
                  <i class="fas fa-search" aria-hidden="true"></i>
                </span>
              </p>
            </div>
            <p class="panel-tabs">
              <a class="is-active">All</a>
              <a>Public</a>
              <a>Private</a>
              <a>Sources</a>
              <a>Forks</a>
            </p>

            <div class="panel-block">
              <button class="button is-link is-outlined is-fullwidth">
                Reset all filters
              </button>
            </div>
          </nav>


@endsection
