<div class="box">
    <div class="panel">
        <div class="panel-heading">
            <h2 class="subtitle is-3">Filters</h2>
        </div>
        <p class="panel-tabs">
            <a class="is-active">Category</a>
            <a>Platforms</a>
            <a>Projects</a>
            <a>My system</a>
        </p>



        <div class="panel-block">
            <div class="field has-addons">
                <div class="control">
                    <label for="searchfield">Search for campaigns</label>
                    <input list="campaignlist" name="searchfield" id="searchfield" class="input">
                    <div class="control">
                        <a class="button is-info">Search</a>
                    </div>
                    <datalist name="campaignlist" id="campaignlist" >
                        @foreach($campaigns as $campaign)
                            <option value="{{ $campaign->slug }}">{{ $campaign->title }}</option>
                        @endforeach
                    </datalist>
                </div>
            </div>
        </div>
        <div class="panel-block">
            <fieldset name="application-type">
                <legend><h4 class="subtitle is-4">Filters</h4></legend>
                @foreach($campaigntags as $categoryKey => $tags)
                    <legend>{{ $categoryKey }}</legend>
                    @foreach($tags as $tagKey => $tag)
                        <div class="field">
                            <div class="control">
                                <label class="fakecheckbox checkbox" for="{{ $tagKey }}">
                                    <span class="checkbox">
                                        <svg class="checked {{ $tagKey }}" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
                                        </svg>
                                    </span>
                                    {{ $tag['display'] }}
                                </label>
                            </div>
                        </div>
                    @endforeach
                @endforeach
            </fieldset>
        </div>
        <div class="panel-block">
            <div class="field has-addons">
                <div class="control is-expanded">
                    <div class="select is-info">
                        <select is-fullwidth>
                            <option>Select dropdown 2</option>
                            <option>With options</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
