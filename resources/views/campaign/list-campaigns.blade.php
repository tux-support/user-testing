<div class="box">
    <div class="container tags">
        @foreach($campaigntags as $tags)
            @foreach($tags as $tagKey => $tag)
            <span class="tag is-danger {{ $tagKey }}">{{ $tag['display'] }}<label for="{{ $tagKey }}"><span class="delete is-small"></span></label></span>
            @endforeach
        @endforeach
    </div>
    <div class="columns flex-wrap is-multiline cards-container">
        @foreach($campaigns as $campaign)
        @php
            $tag = $arraykeys[rand(0,3)];
        @endphp
        <div
            class="column is-6 is-6-desktop is-4-widescreen is-3-fullhd {{ $tag }} tagged card-container">
            <div class="card" >
                <div class="card-header">
                    <p class="card-header-title is-centered">
                        <a href="https://bulma.io/">{{ $campaign->title }}</a>
                    </p>
                </div>
                <div class="card-image">
                    <figure class="image is-3by2">
                        <img src="https://picsum.photos/300/200"
                            alt="Megaphone, people and a computer">
                    </figure>
                </div>
                <div class="card-content">
                    <p class="content">{{ $campaign->description }}</p>
                </div>
                <div class="card-content tags">
                    <p class="content">
                        @if($campaign->submission_start_time > now())
                            Submissions open <time datetime="{{ $campaign->submission_start_time }}">{{ \Carbon\Carbon::parse($campaign->submission_start_time)->diffForHumans(now(), ['syntax' => \Carbon\CarbonInterface::DIFF_RELATIVE_TO_NOW]) }}</time>
                        @else
                            @if($campaign->submission_end_time > now())
                            <em class="has-text-success">Open for submissions!</em><br/>
                                Submissions close <time datetime="{{ $campaign->submission_end_time }}">{{ \Carbon\Carbon::parse($campaign->submission_end_time)->diffForHumans(now(), ['syntax' => \Carbon\CarbonInterface::DIFF_RELATIVE_TO_NOW]) }}</time>
                            @else
                                <span class="has-text-danger">Closed for submissions</span>
                            @endif
                        @endif
                      </p>
                </div>
                <div class="card-content tags">
                    <p class="content"><span class="tag">
                        {{ $campaigntags['Device Type'][$tag]['display'] }}
                      </span></p>
                </div>
                <footer class="card-footer">
                    <a href="https://bulma.io/"
                        class="card-footer-item has-text-centered">
                        <span class="icon is-medium">
                            <svg xmlns="http://www.w3.org/2000/svg"
                                fill="none" viewBox="0 0 24 24"
                                stroke="currentColor">
                                <path strokeLinecap="round"
                                    strokeLinejoin="round"
                                    strokeWidth={2}
                                    d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14" />
                            </svg>
                        </span>
                        To campaign
                    </a>
                </footer>
            </div>
        </div>
        @endforeach
    </div>
</div>
