@extends('layouts.app')

@section('breadcrumbs')
<li class="is-active"><a href="{{ Route::current()->uri() }}"
        aria-current="page">Campaigns</a></li>
@endsection

@section('content-full')
<style>
    /*
    #filters .field, #filters .field select, #filters fieldset, #filters legend {
        width: 100%;
    }
    */

    #filters legend {
        text-align: center;
        padding: 0.5ex;
    }

    /* .tagged {
        max-height: 450px;
    } */

    /*
    Make sure card content stacks neatly on top of eachother
    */
    .card-container {
        max-width: 40ex;
    }

    .cards-container {
        justify-content: space-evenly;
    }

    .card {
        display: flex;
        flex-direction: column;
        align-items: stretch;
        align-content: stretch;
        justify-content: flex-start;
        flex-wrap: nowrap;
    }

    .card > *, .card .image {
        width: 100%;
    }

    .card-header { height: 10ex;  align-self: flex-start; }

    .card-image { display: flex; align-self: flex-start;}

    .card-image .image { height: 100%;}

    .card-content { flex-grow: 2; }
    .card-content.tags { flex-grow: 0; align-self: flex-end; margin: 0; padding-top: 0px; padding-bottom: 0px ;}

    .card-footer {align-self: flex-end; height: 6ex;}

    /*
    Tag controller is a hidden checkbox for each tag,
    that is used to display and hide elements without javascript
    */
    .tag-ctrl {
        visibility: hidden;
        position: absolute;
    }

    /*
    Since a label can only control one checkbox and we need that checkbox
    to be an ancestor of both the visible checkbox and the tagged element
    that is being displayed or hidden, we need to make a fake checkbox,
    so the user gets a visual indication of the current checkbox status
     */
    .fakecheckbox .checkbox {
        display: inline-block;
        border: 1px solid lightgray;
        border-radius: 4px;
        height: 24px;
        width: 24px;
        overflow: hidden;
        vertical-align: sub;
        box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, 0.2);
        margin-right: 0.5ex;
    }
    .fakecheckbox .checked {
        stroke-width: 3;
    }
</style>

    @php
        // This should definitely be cached and optimized
        // Also, I am not doing it this way because I am crazy,
        // but because using @php or @foreach inside <style>
        // tags breaks the Blade formatter in VS Code

        // My attempt at caching this
        $cachedCSSTagsFilter = cache('cached-css-tags-filter', function() use ($campaigntags) {
            $hideTags = [];
            $displayTags = [];
            foreach ($campaigntags as $tags) {
                foreach ($tags as $tagKey => $tag) {
                    $hideTags[] = "#$tagKey:not(:checked) ~ * ." . $tagKey;
                    $displayTags[] = "#$tagKey:checked ~ * ." . $tagKey;
                }
            }
            return "<style>\n"
            . implode(', ', $hideTags)
            . " { display: none; }\n"
            . implode(', ', $displayTags)
            . " { display: flex; }\n"
            . '</style>';
        });

    @endphp
    {!! $cachedCSSTagsFilter !!}
    <div class="container is-fluid">
        <div class="columns">
            <div class="column is-6 is-5-desktop is-4-widescreen is-3-fullhd" style="max-width: 50ex;">
            </div>
            <div class="column">
                        <h1 class="title is-1">Campaigns</h1>
                        <div class="box">
                            <p class="content">
                                Are you looking to try out new software? Are you interested
                                in assisting developers by providing feedback? Find current
                                user testing campaigns here!
                            </p>
                        </div>
            </div>
        </div>

    @foreach($campaigntags as $tags)
            @foreach($tags as $tagKey => $tag)
                <input type="checkbox" id="{{ $tagKey }}" name="{{ $tagKey }}" class="tag-ctrl">
            @endforeach
    @endforeach

    <div class="columns" id="filters">
        <div class="column is-6 is-5-desktop is-4-widescreen is-3-fullhd" style="max-width: 50ex;">
            @include('campaign.filter-sidebar')
        </div>
        <div class="column">
            @include('campaign.list-campaigns')
        </div>

    </div>

</div>

<script>
    let filterTagControllers = document.getElementsByClassName("filter-tag-controller");
    console.log(filterTagControllers);
    var i;
    for (i = 0; i < filterTagControllers.length; i++) {
        filterTagControllers[i].style.backgroundColor = "red";
    }

    let toggleTag = function() {
        let tagName = this.getAttribute("name");
        let display = this.checked;
        console.log(display);
        let elements = document.getElementsByClassName(tagName);
        for (let i = 0; i < elements.length; i++) {
            elements[i].style.display = display?"block":"none";
        }
    };

    for (let i = 0; i < filterTagControllers.length; i++) {
        filterTagControllers[i].addEventListener('click', toggleTag, false);
    }
</script>

@endsection
