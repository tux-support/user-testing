@extends('layouts.app')

@section('breadcrumbs')
    <li class="is-active"><a href="{{ Route::current()->uri() }}" aria-current="page">Organizations</a></li>
@endsection

@section('content')

<div class="container is-fluid">
    <div class="columns">
        <div class="column is-6 is-5-desktop is-4-widescreen is-3-fullhd" style="max-width: 50ex;">
        </div>
        <div class="column">
                    <h1 class="title is-1">Organizations</h1>
                    <div class="box">
                        <p class="content">
                            Organizations, organizations and more organizations.
                        </p>
                    </div>
        </div>
    </div>

    <div class="columns" id="filters">
        <div class="column is-6 is-5-desktop is-4-widescreen is-3-fullhd" style="max-width: 50ex;">

            <div class="box">
                <form action="{{ route('organization.index') }}" method="GET">
                    <div class="panel">
                        <div class="panel-heading">
                            <h2 class="subtitle is-3">Find organization</h2>
                        </div>
                        <div class="panel-block">
                            <div class="field has-addons">
                                <div class="control">
                                    <label for="searchfield">Organization name</label>
                                    <input type="text" name="q" id="searchfield" class="input" placeholder="searchphrase" value="{{ $q }}">
                                    <div class="control">
                                        <input type="submit" class="button is-primary" value="{{ __('Search') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
        <div class="column">
            @if(isset($organizations) && $organizations)
                @foreach($organizations as $organization)
                    <div class="card">
                        <div class="card-content">
                            <a href="{{ route('organization.show', $organization->slug) }}" class="" style="display: block;">{{ $organization->name }}</a>
                        </div>
                    </div>
                @endforeach
            @else

            @endif
        </div>

    </div>

</div>

@endsection
