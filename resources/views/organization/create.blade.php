@extends('layouts.app')

@section('content')
<section class="section">
    <form action="{{ route('organization.store') }}" method="post">
        @method('PUT')
        @csrf

        <div class="container">
            <h1 class="title is-1">New Organization</h1>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <p>There were errors in your form:</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="field">
                <label class="label">Name</label>
                <div class="control">
                    <input class="input @if(old('name'))
                        @if($errors->has('name'))
                            is-danger
                        @else
                            is-success
                        @endif
                    @endif" type="text" placeholder="Text input" name="name" value="{{ old('name') }}">
                </div>
                @if($errors->has('name'))
                    <div>
                        @foreach ($errors->get('name') as $message)
                            <div class="error">{{ $message }}</div>
                        @endforeach
                    </div>
                @endif
            </div>
            <div class="field">
                <label class="label">Description</label>
                <div class="control">
                  <textarea class="textarea @if(old('description'))
                  @if($errors->has('description'))
                      is-danger
                  @else
                      is-success
                  @endif
              @endif" type="text" placeholder="Organization description" name="description">{{ old('description') }}</textarea>
                </div>
                @if($errors->has('description'))
                    <div>
                        @foreach ($errors->get('description') as $message)
                            <div class="error">{{ $message }}</div>
                        @endforeach
                    </div>
                @endif
            </div>
            <div class="field">
                <input type="submit" class="submit button is-primary" value="Create organization">
            </div>
        </div>

    </form>
</section>


@endsection
