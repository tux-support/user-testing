@extends('layouts.app')

@section('breadcrumbs')
<li class=""><a href="{{ route('organization.index') }}" aria-current="page">Organizations</a></li>
<li class="is-active"><a href="{{ Route::current()->uri() }}" aria-current="page">{{ $organization->title }}</a></li>
@endsection

@section('content')


        <div class="container">
            <div class="columns is-8" style="align-items: stretch">
                <div class="column is-three-quarter">
                    <div class="box" style="min-height: 100%;">
                        <h1 class="title is-1">{{ $organization->name }}</h1>
                        @if($organization->projects)
                            <h3 class="title is-3">Current projects</h3>
                            <ul>
                                @foreach($organization->projects as $project)
                                    <a href="{{ route('project.show', $project->slug) }}">{{ $project->title }}</a>
                                    @if($project->campaign)
                                        <p class="content">
                                            @foreach($project->campaign as $campaign)
                                                <a href="{{ route('campaign.show', $campaign->slug) }}">{{ $campaign->title }}</a>
                                            @endforeach
                                        </p>
                                    @else
                                        <p class="content">
                                            No campaigns found
                                        </p>
                                    @endif
                                @endforeach

                            </ul>
                        @else
                            <h3 class="title">No projects at the moment</h3>
                        @endif
                    </div>
                </div>
                <div class="column is-one-quarter">
                    <div class="box" style="min-height: 100%;">
                        @if($teams = $organization->teams()->get())
                            @foreach($teams as $team)
                                <h2 class="title is-2 has-text-centered has-text-primary"
                                    style="text-align: center;">{{ $team->display_name }}</h2>
                                <ul class="block-list">
                                    @foreach($team->members()->get() as $member)
                                        <li>
                                            <a href="{{ route('profile.show', $member->id) }}"
                                            class="">{{ $member->name }}</a>
                                            <p>{!! implode('<br>', $member->roles) !!}</p>
                                        </li>
                                    @endforeach
                                </ul>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>

@endsection
