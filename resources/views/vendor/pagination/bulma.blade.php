@if ($paginator->hasPages())

    <nav class="pagination" role="navigation" aria-label="pagination">

            {{-- Previous Page Link --}}
                    <a href="{{ $paginator->previousPageUrl() }}"
                        class="pagination-previous" @if ($paginator->onFirstPage()) aria-disabled="true" disabled="disabled" @endif rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>

            {{-- Next Page Link --}}
                    <a href="{{ $paginator->nextPageUrl() }}" class="pagination-previous" @if (!$paginator->hasMorePages()) aria-disabled="true" disabled="disabled" @endif rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>

            <ul class="pagination-list">

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li><span class="pagination-link disabled" aria-disabled="true">{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li aria-current="page"><span class="pagination-link is-current">{{ $page }}</span></li>
                        @else
                            <li><a href="{{ $url }}" class="pagination-link">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach


        </ul>
    </nav>
@endif
