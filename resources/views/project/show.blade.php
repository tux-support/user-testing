@extends('layouts.app')

@section('breadcrumbs')
<li class=""><a href="{{ route('organization.index') }}" aria-current="page">Organizations</a></li>
<li class="is-active"><a href="{{ route('organization.show', $project->organization->id) }}" aria-current="page">{{ $project->organization->title }}</a></li>
<li class="is-active"><a href="{{ Route::current()->uri() }}" aria-current="page">{{ $project->title }}</a></li>
@endsection

@section('content')


        <div class="container">
            <div class="columns is-8" style="align-items: stretch">
                <div class="column is-three-quarter">
                    <div class="box" style="min-height: 100%;">
                        <h1 class="title is-1">{{ $project->title }}</h1>
                        @if($project->campaigns)
                            <h3 class="title is-3">Current campaigns</h3>
                            <ul>
                                @foreach($project->campaigns as $campaign)
                                    <a href="{{ route('campaign.show', $campaign->slug) }}">{{ $campaign->title }}</a>
                                    <p class="content">Number of participants: {{ $campaign->team->members()->count() }}</p>
                                @endforeach

                            </ul>
                        @else
                            <h3 class="title">No campaigns at the moment</h3>
                        @endif
                    </div>
                </div>
                <div class="column is-one-quarter">
                    <div class="box" style="min-height: 100%;">
                        @if($members = $project->team->members()->get())
                            <h2 class="title is-2 has-text-centered has-text-primary" style="text-align: center;">Members ({{ $members->count() }})</h2>
                            <ul class="block-list">
                                @foreach($members as $member)
                                    @if($member)
                                            <li><a href="{{ route('profile.show', $member->id) }}" class="">{{ $member->name }}</a></li>
                                    @else
                                        <li>Not a member</li>
                                    @endif
                                @endforeach
                            </ul>
                        @else
                            <h3 class="title">No teams found</h3>
                        @endif
                    </div>


                </div>
            </div>
        </div>

@endsection
