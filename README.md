User Testing
===

This is a proof of concept for a user testing site.

There will be a Social Hub, for advertising your project and testing campaigns, as well as a User Testing application, that is used for developers and users to interact with each other.

The project discussion can be found at [DLN Community Project - Quality Control Platform](https://discourse.destinationlinux.network/t/dln-community-project-quality-control-platform/2475).