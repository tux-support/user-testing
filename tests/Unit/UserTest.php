<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Models\User;

class UserTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testAddUser()
    {
        $user = User::create([
            'name' => 'My test name',
            'email' => 'my-mail@example.com'
        ]);
        $this->assertTrue(User::firstWhere('id', $user->id));
    }
}
